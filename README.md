﻿#Mentoring Support System
Delivery Project Charter
##Business Context
aka System context aka Using System definition;
Career path for senior professional cannot be planned based on a standard pattern;
Mentoring from a person who is successfully got to desired position is the most efficient way to develop a career for such a pro;
Key challenges related to Mentoring application are:
* Find a direction to grow;
* Find a role models to follow;
* Find and get to agreement with a Mentor;
* Find a right questions to ask;
* Find a right advice to give;
* Be able to maximize a value from the Mentoring process;
There are many documents, articles, mentoring SMEs - but almost no open and free systems to support the process and address the challenges;
##Goals and Objectives
The project of delivering “Mentoring Support System” aka “MSS” has the following goals:
1. Primary: facilitate Mentoring activities for the Project team (aka Enabling System Stakeholders)
2. Secondary: apply Systems Thinking to the full Project lifecycle and related activities to get experience and elaborate “best practices” aka adopt methodology for a class of similar projects (open info, open community, open source);
3. Tertiary: implement and deliver technology to support Mentoring discipline;
##Driving Principles
Open Information: all the information related to the Project is open for Community; participation in the Project activities means the acceptance of this principle. Such information include but not limited to:
* Project work products: plan, status, design documents, source code, decisions log, communication notes and MFUs
* Domain information: competencies/skills ontologies, rankings, methodologies
* User information: participants career facts/timelines/resources, user content
* System information: correlations, links, derived facts, trends, recommendations
##Related Resources
* Slack: TBD
* Trello: TBD
* Knowledge Base: https://mentoringsystem.atlassian.net

License: https://opensource.org/licenses/MIT
